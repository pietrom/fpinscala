package com.darwinsw.learning.hello

object Curry {
  def curry[A, B, C](f: (A, B) => C) : A => (B => C) = {
    //(a : A) => (b: B) => f(a, b)

      def x(a: A) = {
        //(b: B) => f(a, b)
        def y(b:B) = f(a, b)
        y _
      }
    x _
  }

  def uncurry[A, B, C](f: A => B => C)  = (a: A, b: B) => f(a)( b)
}
