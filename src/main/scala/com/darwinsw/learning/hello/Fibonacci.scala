package com.darwinsw.learning.hello

import scala.annotation.tailrec

object Fibonacci {
	def fib(n : Int) : Int = {
		if(n <= 1) 1
		else {
			@tailrec
			def loop(i : Int, curr: Int, prev: Int) : Int = {
				if(i <= 1) curr
				else loop(i - 1, curr + prev, curr)
			}
			loop(n, 1, 1)
		}
	}
}