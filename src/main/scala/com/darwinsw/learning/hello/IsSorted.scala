package com.darwinsw.learning.hello

import scala.annotation.tailrec

object IsSorted {
  def isSorted[A](xs : Array[A], ordering: (A, A) => Int) : Boolean = {
    @tailrec
    def loop(as: Array[A], prev:A) : Boolean = {
      if(as.isEmpty) true
      else if(ordering(as.head, prev) < 0) false
      else loop(as.tail, as.head)
    }
    if(xs.isEmpty) true
    else loop(xs.tail, xs.head)
  }
}
