package com.darwinsw.learning.hello

import com.darwinsw.learning.hello.Fibonacci.fib
import org.scalatest.{FlatSpec, Matchers}

class FibonacciSpec extends FlatSpec with Matchers {
  "First sequence value" should "be 1" in {
    fib(0) should be (1)
  }

  "Second sequence value" should "be 1" in {
    fib(1) should be (1)
  }

  "fib(5)" should "be 8" in {
    fib(5) should be (8)
  }

  "fib(6)" should "be 13" in {
    fib(5) should be (8)
  }

  "fib(7)" should "be 21" in {
    fib(5) should be (8)
  }

  "fib(8)" should "be 34" in {
    fib(5) should be (8)
  }
}
