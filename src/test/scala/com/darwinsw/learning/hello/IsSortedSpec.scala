package com.darwinsw.learning.hello

import org.scalatest.{FlatSpec, Matchers}
import IsSorted.isSorted

class IsSortedSpec extends FlatSpec with Matchers {
  // isSorted(Array(1, 3, 5, 7), (x: Int, y: Int) => x > y) shouldBe
  //
  //isSorted(Array(7, 5, 1, 3), (x: Int, y: Int) => x < y) shouldBe
  //
  //isSorted(Array("Scala", "Exercises"), (x: String, y: String) => x.length > y.length) shouldBe

  "1, 3, 5, 7" should "be sorted" in {
    isSorted(Array(1, 3, 5, 7), (x: Int, y: Int) => x - y) should be (true)
  }

  "7, 5, 1, 3" should "not be sorted" in {
    isSorted(Array(7, 5, 1, 3), (x: Int, y: Int) => x - y) should be (false)
  }

  "7, 5, 1, 3" should "be sorted with custom ordering function" in {
    isSorted(Array(7, 5, 1, 3), (x: Int, y: Int) => -1) should be (false)
  }

  "Empty array" should "be sorted" in {
    isSorted(Array(), (x: Int, y: Int) => x - y) should be (true)
  }
}
