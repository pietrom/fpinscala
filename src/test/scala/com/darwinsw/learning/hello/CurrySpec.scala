package com.darwinsw.learning.hello

import org.scalatest.{FlatSpec, Matchers}

class CurrySpec  extends FlatSpec with Matchers{
  def f(a: Int, b: Int): Int = a + b
  def g(a: Int)(b: Int): Int = a + b

  "Applying curry" should "return curried function" in {
    Curry.curry(f)(11)(19) should be (30)
  }

  "Applying uncurry" should "return uncurried function" in {
    Curry.uncurry(g)(11, 19) should be (30)
  }

}
