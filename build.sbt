organization := "com.darwinsw.learning"
version := "1.10.0"
// set the Scala version used for the project
scalaVersion := "2.12.4"

EclipseKeys.executionEnvironment := Some(EclipseExecutionEnvironment.JavaSE18)

libraryDependencies += "junit" % "junit" % "4.12" % "test"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test"